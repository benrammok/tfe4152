//-----------------------------------------------------------------------------
//
// Title       : Counter
// Design      : RE_control
// Author      : Benjamin Ramberg M�kleg�rd
// Company     : NTNU
//
//-----------------------------------------------------------------------------
//
// File        : e:\My_Designs\Project\RE_control\src\Counter.v
// Generated   : Sun Nov  4 17:38:36 2018
// From        : interface description file
// By          : Itf2Vhdl ver. 1.22
//
//-----------------------------------------------------------------------------
//
// Description : 
//
//-----------------------------------------------------------------------------
`timescale 1 ms / 1 us

//{{ Section below this comment is automatically maintained
//   and may be overwritten
//{module {Counter}}
module Counter (CLK, RESET, Enable, Increase, Decrease, Ex_time);

//5-bit output from 0 to 31
output [4:0] Ex_time ;
reg    [4:0] Ex_time ;

input CLK ;
wire CLK ;
input RESET ;
wire RESET ;
input Enable ;
wire Enable;
input Increase ;
wire Increase ;
input Decrease ;
wire Decrease ;


reg [4:0] rangeLow =   1;
reg [4:0] rangeHigh = 29;

//}} End of automatically maintained section

// -- Enter your statements here -- //

always @(posedge CLK)
begin
	if (RESET == 1)
		Ex_time <= rangeLow;
	else if (Enable == 1)
		//Increase has Priority over Decrease
		if (Increase & Ex_time != rangeHigh)
			Ex_time <= Ex_time + 1'b1;
		else if (Decrease & Ex_time != rangeLow) 
			Ex_time <= Ex_time - 1'b1;	 	
end


endmodule
