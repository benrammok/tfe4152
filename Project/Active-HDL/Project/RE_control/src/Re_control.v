//-----------------------------------------------------------------------------
//
// Title       : Re_control
// Design      : RE_control
// Author      : Benjamin Ramberg M�kleg�rd
// Company     : NTNU
//
//-----------------------------------------------------------------------------
//
// File        : e:\My_Designs\Project\RE_control\src\Re_control.v
// Generated   : Sun Nov  4 17:24:57 2018
// From        : interface description file
// By          : Itf2Vhdl ver. 1.22
//
//-----------------------------------------------------------------------------
//
// Description : 
//
//-----------------------------------------------------------------------------
`timescale 1 ms / 1 us

module Re_control (CLK, RESET, Init, NRE_1, NRE_2, ADC, Expose, Erase, OVERFLOW, Exp_en, Exp_time, Currentcnt, Start_counter, C_time);

output       NRE_1 ;
reg          NRE_1 ;
output       NRE_2 ;
reg          NRE_2 ;
output       ADC ;
reg          ADC ;
output       Expose ;
reg          Expose ;
output       Erase ;
reg          Erase ;
output       Exp_en;
reg          Exp_en;
output       Start_counter;
reg          Start_counter;
output [4:0] C_time;
reg    [4:0] C_time;


input        CLK ;
wire         CLK ;
input        RESET ;
wire         RESET ;
input [0:4]  Exp_time ;
wire  [0:4]  Exp_time ;
input        Init ;
wire         Init ;	  
input        OVERFLOW;
wire         OVERFLOW;
input [4:0]  Currentcnt;
wire  [4:0]  Currentcnt;

//STATE MACHINE DEFINITIONS

//Define Three States (INIT; EXPOSURE; READOUT)
parameter IDLE = 2'b00, EXPOSURE = 2'b01, READOUT  = 2'b10;

//State Regiters
reg [1:0] state;
reg [1:0] nextstate;

//Sequential Logic for State Registers
always@(posedge CLK)
begin
	state <= nextstate;
end	 

always@(state, Init, OVERFLOW, RESET)
begin
	case(state)
		IDLE    : 
			begin 
				if (RESET != 1)
					nextstate = (Init      == 1 & RESET != 1) ? EXPOSURE : IDLE;
				else
					nextstate = IDLE;
			end
		EXPOSURE:
			begin
				if(RESET != 1)
					nextstate = (OVERFLOW  == 1) ? READOUT  : EXPOSURE;
				else
					nextstate = IDLE;
			end
		READOUT :
			begin
				if(RESET != 1)
					nextstate = (OVERFLOW  == 1) ? IDLE     : READOUT ;
				else
					nextstate = IDLE;
			end
		default : nextstate =  IDLE ;
	endcase	
end

//Combinatorial Logic for each state
always@(*)
begin
	case(state)
		IDLE    :
		begin
			//Logic to controll external Counter + Timer
			Exp_en    <= 1;
			Start_counter <= 0;
			C_time <= Exp_time;
			//Logic Signals to Pixel Cell
			Expose <= 0;
			Erase <= 1;
			ADC <= 0;
			NRE_1 <= 1;
			NRE_2 <= 1;
			
			end
		EXPOSURE:
		begin
			//Logic to controll external Counter + Timer
			Exp_en <= 0;
			Start_counter <= 1;
			C_time <= Exp_time;	
			
			//Logic Signals to Pixel Cell
			Expose <= 1;
			Erase <= 0;
			ADC <= 0;
			NRE_1 <= 1;
			NRE_2 <= 1;
			end
		READOUT :
		begin
			
			//Logic to controll external Counter + Timer
			Exp_en <= 0;
			Start_counter <= 1;
			C_time <= 8;
			
			//Logic Signals to Pixel Cell
			Expose <= 0;
			Erase <= 0;
			case(Currentcnt)
				1: begin NRE_1 <= 0; ADC   <= 0; NRE_2 <= 1; end //Set Readout for first row LOW to indicate read	    
				2: begin NRE_1 <= 0; ADC   <= 1; NRE_2 <= 1; end //Enable ADC sampling
				3: begin NRE_1 <= 0; ADC   <= 0; NRE_2 <= 1; end //Disable ADC sampling
				4: begin NRE_1 <= 1; ADC   <= 0; NRE_2 <= 1; end //Set Readout for first row HIGH to indicate finsihed read
				5: begin NRE_1 <= 1; ADC   <= 0; NRE_2 <= 0; end //Set Readout for second row LOW to indicate read
				6: begin NRE_1 <= 1; ADC   <= 1; NRE_2 <= 0; end //Enable ADC sampling
				7: begin NRE_1 <= 1; ADC   <= 0; NRE_2 <= 0; end //Disable ADC sampling
				8: begin NRE_1 <= 1; ADC   <= 0; NRE_2 <= 1; end //Set Readout for second row HIGH to indicate finsihed read
				default:
				begin
					NRE_1 <= 1;
					NRE_2 <= 1;
					ADC   <= 0;
				end
			endcase	
		end
	endcase
end

endmodule
