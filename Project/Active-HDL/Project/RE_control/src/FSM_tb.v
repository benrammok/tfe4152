//-----------------------------------------------------------------------------
//
// Title       : TopModule Testbench
// Design      : TopModule Testbench 
// Author      : Benjamin Ramberg M�kleg�rd
// Company     : NTNU
//
// Description : 
//
//-----------------------------------------------------------------------------
`timescale 1ms / 1 us

module FSM_tb;

reg          NRE_1 ;
reg          NRE_2 ;
reg          ADC ;
reg          Expose ;
reg          Erase ;

reg          CLK ;
reg          RESET ;
reg          Exp_increase ;
reg          Exp_decrease ;
reg          INIT ;


	
//Device Under Test (DUT)

TopModule TOP(.CLK(CLK), .RESET(RESET), .Exp_increase(Exp_increase), .Exp_decrease(Exp_decrease), .Init(INIT), .NRE_1(NRE_1), .NRE_2(NRE_2), .ADC(ADC), .Expose(Expose), .Erase(Erase));

//Clock Signal
initial    CLK = 0;
always #0.5  CLK = ~CLK;
	

	 
initial 
begin
	RESET = 1; Exp_increase = 0; Exp_decrease = 0; INIT = 0; # 1;
	RESET = 0; # 1;
	INIT  = 1; # 0.5;
	INIT  = 0; # 15;
	//After 2ms + 9ms = 11ms we are back in IDLE
	Exp_increase = 1; Exp_decrease = 1; #30; //Wait for 30
	Exp_increase = 0; Exp_decrease = 0; #0.5;
	INIT = 1; # 0.5;
	INIT = 0;
end

initial #100 $finish;


endmodule