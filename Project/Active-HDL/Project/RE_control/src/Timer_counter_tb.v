//-----------------------------------------------------------------------------
//
// Title       : Counter Testbench
// Design      : Counter Testbench 
// Author      : Benjamin Ramberg M�kleg�rd
// Company     : NTNU
//
// Description : 
//
//-----------------------------------------------------------------------------
`timescale 1ms / 1 us

module Timer_counter_tb;

reg         CLK       ;
reg         RESET     ;
reg         Start     ;
reg  [4:0]  Initial  ;

reg  [4:0]  Count    ;
reg         OVERFLOW ;
	
//Device Under Test (DUT)
Timer_counter T1(.CLK(CLK), .RESET(RESET), .Initial(Initial), .Start(Start), .Count(Count), .OVERFLOW(OVERFLOW));


//Clock Signal
initial      CLK = 0;
always #0.5  CLK = ~CLK;
	

	 
initial 
begin
	RESET = 1; Initial = 1; Start = 0; # 1;
	RESET = 0; # 1;
	Start = 1; # 3;
	Start = 0; # 1;

end

initial #10 $finish;


endmodule