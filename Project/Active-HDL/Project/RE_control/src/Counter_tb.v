//-----------------------------------------------------------------------------
//
// Title       : Counter Testbench
// Design      : Counter Testbench 
// Author      : Benjamin Ramberg M�kleg�rd
// Company     : NTNU
//
// Description : 
//
//-----------------------------------------------------------------------------
`timescale 1ms / 1 us

module Counter_tb;

//5-bit reg for holding output value
reg    [4:0] Ex_time ;

reg Increase ;
reg Decrease ;
reg CLK ;
reg RESET ;
reg Enable;


	
//Device Under Test (DUT)
Counter C1(.Increase(Increase), .Decrease(Decrease), .CLK(CLK), .RESET(RESET), .Ex_time(Ex_time), .Enable(Enable));


//Clock Signal
initial      CLK = 0;
always #0.5  CLK = ~CLK;
	

	 
initial 
begin
	RESET = 1; Increase = 0; Decrease = 0; Enable = 0; # 1;
	RESET = 0; # 1;
	Increase = 1; # 1;
	Enable = 1; # 30;
	Increase = 0; Decrease = 1; # 30;
end

initial #10 $finish;


endmodule