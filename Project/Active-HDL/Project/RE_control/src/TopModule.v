//-----------------------------------------------------------------------------
//
// Title       : TopModule
// Design      : TopModule
// Author      : Benjamin Ramberg M�kleg�rd
// Company     : NTNU
//
// Description : 
//
//-----------------------------------------------------------------------------
`timescale 1 ms / 1 us

module TopModule (CLK, RESET, Exp_increase, Exp_decrease, Init, NRE_1, NRE_2, ADC, Expose, Erase);

output       NRE_1 ;
wire         NRE_1 ;
output       NRE_2 ;
wire         NRE_2 ;
output       ADC ;
wire         ADC ;
output       Expose ;
wire         Expose ;
output       Erase ;
wire         Erase ;


input        CLK ;
wire         CLK ;
input        RESET ;
wire         RESET ;
input        Exp_increase ;
wire         Exp_increase ;
input        Exp_decrease ;
wire         Exp_decrease ;
input        Init ;
wire         Init ;

//Wires for connecting devices
wire         ovf ; 
wire         Exp_en ;
wire [4:0]   Exp_time ;
wire [4:0]   C_time ;
wire [4:0]   Ccount   ;
wire         Start_cnt ;


//Setup Counter and Programmable "Timer"/Counter for Exposure/Readout and FSM
Re_control FSM(.CLK(CLK), .RESET(RESET), .Init(Init), .NRE_1(NRE_1), .NRE_2(NRE_2), .ADC(ADC), .Expose(Expose), .Erase(Erase), .OVERFLOW(ovf), .Exp_en(Exp_en), .Exp_time(Exp_time), .Currentcnt(Ccount), .Start_counter(Start_cnt), .C_time(C_time));
Counter C1(.CLK(CLK), .RESET(RESET), .Enable(Exp_en), .Increase(Exp_increase), .Decrease(Exp_decrease), .Ex_time(Exp_time));
Timer_counter T1(.CLK(CLK), .RESET(RESET), .Initial(C_time), .Start(Start_cnt), .Count(Ccount) ,.OVERFLOW(ovf));	
endmodule