//-----------------------------------------------------------------------------
//
// Title       : Timer_counter
// Design      : RE_control
// Author      : Benjamin Ramberg M�kleg�rd
// Company     : NTNU
//
//-----------------------------------------------------------------------------
//
// File        : e:\My_Designs\Project\RE_control\src\Timer_counter.v
// Generated   : Sun Nov  4 18:32:20 2018
// From        : interface description file
// By          : Itf2Vhdl ver. 1.22
//
//-----------------------------------------------------------------------------
//
// Description : 
//
//-----------------------------------------------------------------------------
`timescale 1 ms / 1 us

//{{ Section below this comment is automatically maintained
//   and may be overwritten
//{module {Timer_counter}}
module Timer_counter (CLK, RESET, Initial, Start, Count, OVERFLOW);

output       OVERFLOW ;
reg          OVERFLOW ;
output [4:0] Count    ;
reg    [4:0] Count    ;


input       CLK       ;
wire        CLK       ;
input       RESET     ;
wire        RESET     ;
input  [4:0] Initial  ;
wire   [4:0] Initial  ;
input       Start     ; 
wire        Start     ;

//}} End of automatically maintained section

always @(posedge CLK)
begin
	if (RESET == 1)
		begin
			Count = 0;
		end
	else if(OVERFLOW == 1)
		Count <= 0;
	else if(Start == 1 & OVERFLOW != 1)		 
		Count <= Count + 1'b1;
		
end

always @(Count, Initial)
begin
	if(Count == Initial)
		OVERFLOW = 1;
	else
		OVERFLOW = 0;
end
endmodule
