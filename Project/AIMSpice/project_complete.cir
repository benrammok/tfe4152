[aimspice]
[description]
1883
TFE4152 Design of Integrated Circuit Project Completed


.include E:\AimSpiceFiles\p18_model_card.inc
.include E:\AimSpiceFiles\p18_cmos_models_tt.inc
*.include E:\AimSpiceFiles\PhotoDiode.inc

.param Ipd_1 750pA
.param VDD 1.8 ! Supply

.param EXPOSURETIME = 2ms ! Exposure time, range [2 ms, 30 ms]
.param TRF = {EXPOSURETIME/100} ! Risetime and falltime of EXPOSURE and ERASE signals
.param PW = {EXPOSURETIME} ! Pulsewidth of EXPOSURE and ERASE signals
.param PERIOD = {EXPOSURETIME*10} ! Period for testbench sources
.param FS = 1k; ! Sampling clock frequency 
.param CLK_PERIOD = {1/FS} ! Sampling clock period
.param EXPOSE_DLY = {CLK_PERIOD} ! Delay for EXPOSE signal
.param ERASE_DLY = {6*CLK_PERIOD + EXPOSURETIME} ! Delay for ERASE signal
.param NRE_R1_DLY = {2*CLK_PERIOD + EXPOSURETIME} ! Delay for NRE_R1 signal

.subckt PhotoDiode  VDD N1_R1C1 
I1_R1C1  VDD   N1_R1C1   DC  Ipd_1
d1 N1_R1C1 vdd dwell 1
.model dwell d cj0=1e-14 is=1e-12 m=0.5 bv=40
Cd1 N1_R1C1 VDD 30f
.ends 

VDD vdd 0 1.8
VSS vss 0 0
VIN vin 0 0
Ver Erase 0 0 pulse(0 VDD ERASE_DLY TRF TRF CLK_PERIOD PERIOD)
Vex Expose 0 0 pulse(0 VDD EXPOSE_DLY TRF TRF EXPOSURETIME PERIOD)
VNRE_R1 NRE_R1 0 dc 0 pulse(VDD 0 NRE_R1_DLY TRF TRF CLK_PERIOD PERIOD)

*Parameters for NMOS
.param rn = 2.85714
.param ln = 2u
.param wn = 10u

XP1 vdd photOut PhotoDiode

*M1 and M2 should be equal
*When there is good lighting we expect a current of 
*750pA to flow, if not a current of 50pA will flow.

M1 photOut Expose Vc vss NMOS L=2u W=2u
M2 Vc Erase vss vss NMOS L=2u W=2u 
CS Vc vss 1.5pF 

*Buffer and Output Switch + Active Load
MC1 vdd  vout vout vdd PMOS L=2u W=2u
M4  vout NRE_R1 vm3  vdd PMOS L=0.7u W=2u
M3  vm3  Vc  vss  vdd PMOS L=2u W=2u
Cc1 vout vss 3pF

*.plot tran v(Expose) v(Vc)
.plot V(vout) 
.plot v(Expose) V(NRE_R1) v(Erase)
.plot V(Vc)
[dc]
1
VIN
0
1
0,1
[tran]
0.001
60ms
X
X
0
[ana]
4 0
[end]
